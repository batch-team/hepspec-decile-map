SPECFILE = hepspec-decile-map.spec 
PACKAGE=$(shell grep -s '^Name'    $(SPECFILE) | sed -e 's/Name: *//')
VERSION=$(shell grep -s '^Version' $(SPECFILE) | sed -e 's/Version: *//')
PKGNAME=$(PACKAGE)-$(VERSION)
TARFILE=$(PKGNAME).tar.gz

source:
	@tar cvzf $(TARFILE) --exclude-vcs --transform 's,^,$(PKGNAME)/,' hepspec_decile.py

clean:
	rm *.tar.gz

srpm:   source
	rpmbuild -bs --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

rpm:   source
	rpmbuild -bb --define 'dist $(DIST)' --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

