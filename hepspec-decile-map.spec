Name: hepspec-decile-map
Version: 0.0.3
Release: 2%{?dist}
Summary: HTCondor map of hepspec / decile

Group:     System Environment/Base
License:   MPLv2.0
Source0:   %{name}-%{version}.tar.gz
Requires:  python3-numpy,python3-condor
BuildArch: noarch

%define debug_package %{nil}
%description
HTCondor map of hepspec / decile

%prep
%setup -q

%build

%install
mkdir -p  %{buildroot}/usr/bin
install -p -D -m 755 hepspec_decile.py %{buildroot}/usr/bin

%clean

%files
%defattr(-,root,root,-)
/usr/bin/hepspec_decile.py

%changelog
* Mon Jan 16 2023 Jan van Eldik <Jan.van.Eldik@cern.ch> - 0.0.3-2
- Build for EL9
- Spec file cleanup
* Wed Mar 10 2021 Luis Fernández Álvarez <luis.fernandez.alvarez@cern.ch> - 0.0.3-1
- Build with RPMCI
- Migrate to Python3
- Add CentOS Stream 8 build
