#!/usr/bin/python3

import classad
import htcondor
import numpy
import time
import sys
from argparse import ArgumentParser


def get_pool_hepspec(pool, retry_delay=30, max_retries=4):
    results = list()
    coll = htcondor.Collector(pool)
    retries = 0
    while retries < max_retries:
        try:
            startd_ads = coll.query(htcondor.AdTypes.Startd, True,
                    ['SlotType', 'HEPSPEC', 'TotalCpus'])
        except:
            retries += 1
            startd_ads = None
            time.sleep(retry_delay)
        else:
            break

    if startd_ads is None:
        print(f"Trouble geting pool {pool} startds, giving up.")
        return []

    for a in startd_ads:
        slot_type = a.get("SlotType", "Static")
        
        if slot_type == "Partitionable":
            cpus = a.get("TotalCpus")
            hepspec = a.get("HEPSPEC")
            if not hepspec:
                continue
            slot_hs = hepspec / cpus
            results.append(slot_hs)

    return results

def get_decile(decile, hepspec_list):
    a = numpy.array(hepspec_list)
    return numpy.percentile(a,decile)

def main():
    parser = ArgumentParser(description="deciles for hepspec")
    parser.add_argument('--pool', default="tweetybird03.cern.ch",
            help="pool collector to query")
    pargs = parser.parse_args()
    hs = get_pool_hepspec(pargs.pool)
    for dec in [a*10 for a in range(0,11)]:
        print(f"* {dec} {get_decile(dec,hs)}")

if __name__=='__main__':
    main()
